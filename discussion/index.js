// console.log("Hello");

// Function Parameters and Arguments

// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
// Functions are mostly created to create complicated tasks to run several lines of code in succession
// They are also used to prevent repeating lines/blocks of codes that perform the same task/function
//We also learned in the previous session that we can gather data from user input using a prompt() window.

function printInput() {
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, " + nickname);
}

// printInput();

// Complete function -> with parameter and argument

function printName(name) {
	console.log("My name is " + name);
}

// Parameter -> "name" -> acts as a variable or container -> we can store data here

printName("Juana");
printName("John");
printName("Jane");

let sampleVariable = "Yui";

printName(sampleVariable);

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);

// Functions as Arguments

function argumentFunction() {
	console.log("This function was passed as an argument before the message was printed.");
}

function invokeFunction(argumentFunction) {
	argumentFunction();
}

invokeFunction(argumentFunction);

console.log(argumentFunction);

// Using Multiple Parameters

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
};

createFullName("Juan", "Dela", "Cruz");

// "Juan" will be stored in the parameter "firstName"
// "Dela" will be stored in the parameter "middleName"
// "Cruz" will be stored in the parameter "lastName"

createFullName("Juan", "Dela");
createFullName("Juan", "Dela", "Cruz", "Hello");

// Using variables as arguments

let firstName = "John", middleName = "Doe", lastName = "Smith";

createFullName(firstName, middleName, lastName);

function printFullName(middleName, firstName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("Juan", "Dela", "Cruz");

// Using alert() -> allows us to show a small window at the top of the browser to show information.

alert("Hello World");

function showSampleAlert() {
	alert("Hello, User!");
}

showSampleAlert();

console.log("I will only log in the console when the alert is dismissed.");

//Notes on the use of alert():
//Show only an alert() for short dialogs/messages to the user. 
//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

function printWelcomeMessage() {
	let firstName = prompt("Enter your first name");
	let lastName = prompt("Enter your last name");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();